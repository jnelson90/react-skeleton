import path from 'path';
import { ChunkExtractor } from '@loadable/server';
import applicationRenderer from './lib/render/application';
import pageRenderer from './lib/render/page';
import paths from '../webpack/paths';

const clientStatsFile = path.resolve(`${paths.BUILD_DIR}/loadable-stats.json`);

export default () => ((req, res) => {
	// Create a Loadable Server file chunk extractor from the statsFile
	const chunkExtractor = new ChunkExtractor({ statsFile: clientStatsFile });

	// Render the application to a string using the chunk extractor
	const {
		applicationString, scriptTags, styleTags, cssString
	} = applicationRenderer(req.url, chunkExtractor);

	// Render the page markup
	pageRenderer({
		applicationString, // Rendered React App
		preloadedState: {},
		scriptTags, // List of JS script tags
		styleTags, // List of CSS style tags
		cssString // Long CSS string to inline
	}).then((htmlMarkup) => {
		res.send(htmlMarkup);
	});
});
