const webpack = require('webpack');
const { mergeWithCustomize, customizeArray } = require('webpack-merge');
const common = require('./client.common');

const configuration = mergeWithCustomize({
	customizeArray: customizeArray({
		'entry.main': 'prepend'
	})
})(common, {
	mode: 'development',
	entry: ['webpack-hot-middleware/client','./app/main.js'],
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development')
			}
		}),
		new webpack.HotModuleReplacementPlugin()
	]
});

module.exports = configuration;
