import React from 'react';

import './TestPage.scss';

function TestPage() {
	return (
		<div className="test-page">
			This is a test page
		</div>
	);
}

export default TestPage;
