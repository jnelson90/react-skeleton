import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { HomePage, TestPage } from 'pages/Bundles';

export const staticRoutes = [
	{
		path: '/',
		component: HomePage
	},
	{
		path: '/test',
		component: TestPage
	}
];

export default function Routes() {
	return (
		<Switch>
			{staticRoutes.map((route, i) => (
				<Route
					key={route.key || i}
					exact
					strict
					{...route}
				/>
			))}
		</Switch>
	);
}
