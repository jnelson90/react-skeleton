import React from 'react';
import { shallow } from 'enzyme';
import TestPage from '../TestPage';

describe('Pages | TestPage', () => {
	it('renders the TestPage', () => {
		const component = shallow(
			<TestPage />
		);

		expect(component).toMatchSnapshot();
	});
});
