const webpack = require('webpack');
const paths = require('./paths');
const imageLoaders = require('./image-loaders');

const configuration = {
	name: 'server',
	target: 'node',
	mode: 'development',
	devtool: 'source-map',
	entry: [`${paths.SERVER_DIR}/render.js`],
	output: {
		path: paths.SERVER_BUILD_DIR,
		filename: '[name].js',
		libraryTarget: 'commonjs2',
		publicPath: '/static/'
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('development')
			}
		})
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /\.s[ac]ss$/i,
				loader: 'css-loader',
				options: {
					modules: {
						exportOnlyLocals: true
					}
				}
			},
			...imageLoaders
		]
	}
};


module.exports = configuration;
