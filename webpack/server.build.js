const webpack = require('webpack');
const LoadablePlugin = require('@loadable/webpack-plugin');
const paths = require('./paths');
const hash = require('./hash');
const imageLoaders = require('./image-loaders');

const publicPath = `https://static.storage.com/assets/${hash}/`;
const target = 'node';

module.exports = {
	name: 'server',
	target,
	mode: 'production',
	devtool: 'source-map',
	entry: [`${paths.SERVER_DIR}/render.js`],
	externals: ['@loadable/component'],
	output: {
		path: paths.SERVER_BUILD_DIR,
		filename: '[name].js',
		libraryTarget: 'commonjs2',
		publicPath
	},
	plugins: [
		new webpack.optimize.LimitChunkCountPlugin({
			maxChunks: 1
		}),
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		}),
		new LoadablePlugin()
	],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						caller: { target }
					}
				}
			},
			{
				test: /\.s[ac]ss$/i,
				loader: 'css-loader',
				options: {
					modules: {
						exportOnlyLocals: true
					}
				}
			},
			...imageLoaders
		]
	},
	optimization: {
		minimize: false
	}
};
