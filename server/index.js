/* eslint-disable import/no-extraneous-dependencies, import/no-dynamic-require, global-require */
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const { WebServer } = require('@sparefoot/webserver');
const config = require('./config');
const clientConfig = require('../webpack/client.dev');
const serverConfig = require('../webpack/server.dev');
const paths = require('../webpack/paths');

const DEV = process.env.NODE_ENV === 'development';
const { publicPath } = clientConfig.output;

// Create the base web server
const app = WebServer.create({
	environment: config.env,
	forceHttps: true,
	...config.webserver
});

// Prevents WebServer from restarting on hot reloads
let built = false;
const done = () => !built && app.startServer(() => { built = true; });

if (DEV) {
	const compiler = webpack([clientConfig, serverConfig]);
	const clientCompiler = compiler.compilers.find((child) => child.name === 'client');
	const options = {
		publicPath,
		serverSideRender: true,
		stats: { colors: true },
		writeToDisk(filePath) {
			return /server-dist/.test(filePath) || /loadable-stats/.test(filePath);
		}
	};

	app.init((server) => {
		server.use(webpackDevMiddleware(compiler, options)); // Serves emitted webpack files to Express server
		server.use(webpackHotMiddleware(clientCompiler)); // Enable webpack hot reloading of client bundle
		server.use(webpackHotServerMiddleware(compiler, { // Enables server hot reloading of server bundle
			serverRendererOptions: { config }
		}));
	});

	compiler.hooks.done.tap('MyPlugin', done);
} else {
	const serverRender = require(`${paths.SERVER_BUILD_DIR}/main.js`).default;
	app.init((server) => {
		server.use(serverRender());
	});
	done();
}
