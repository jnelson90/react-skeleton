const LoadablePlugin = require('@loadable/webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const paths = require('./paths');
const imageLoaders = require('./image-loaders');

const target = 'web';

module.exports = {
	name: 'client',
	mode: 'production',
	target,
	entry: './app/main.js',
	output: {
		filename: 'bundle.js',
		path: paths.BUILD_DIR,
		publicPath: '/static/'
	},
	devtool: 'inline-source-map',
	plugins: [new LoadablePlugin(), new MiniCssExtractPlugin()],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /nodes_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						caller: { target }
					}
				}
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							sassOptions: {
								includePaths: [`${paths.APP_DIR}/styles`]
							}
						}
					}
				]
			},
			...imageLoaders
		],
	},
	optimization: {
		minimize: false
	}
};
