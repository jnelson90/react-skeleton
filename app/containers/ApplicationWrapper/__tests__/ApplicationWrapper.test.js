import React from 'react';
import { shallow } from 'enzyme';
import ApplicationWrapper from '../ApplicationWrapper';

describe('Containers | ApplicationWrapper', () => {
	it('renders the ApplicationWrapper', () => {
		const component = shallow(
			<ApplicationWrapper />
		);

		expect(component).toMatchSnapshot();
	});
});
