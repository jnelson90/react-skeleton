import React from 'react';
import { shallow } from 'enzyme';
import HomePage from '../HomePage';

describe('Pages | HomePage', () => {
	it('renders the HomePage', () => {
		const component = shallow(
			<HomePage />
		);

		expect(component).toMatchSnapshot();
	});
});
