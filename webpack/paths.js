const path = require('path');

module.exports = {
	PROJECT_DIR: path.resolve(__dirname, '../'),
	APP_DIR: path.resolve(__dirname, '../app/'),
	SERVER_DIR: path.resolve(__dirname, '../server/'),
	BUILD_DIR: path.resolve(__dirname, '../dist/'),
	SERVER_BUILD_DIR: path.resolve(__dirname, '../server-dist/')
};
