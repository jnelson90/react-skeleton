const { Config } = require('@sparefoot/config');

module.exports = (Override) => new Config({
	serviceName: 'storage',
	port: Override({
		value: 9133,
		env: 'WEBSERVER_PORT'
	}).int(),
	logFormat: {
		name: 'json'
	},
	siteId: Override({
		value: 666,
		env: 'SITE_ID'
	}).int(),
	mobileSiteId: Override({
		value: 777,
		env: 'MOBILE_SITE_ID'
	}).int(),
	baseUrl: Override({
		value: 'http://localhost:9133',
		env: 'WEBSERVER_BASE_URL'
	}).string()
});
