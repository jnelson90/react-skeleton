import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { ChunkExtractorManager } from '@loadable/server';
import { ApplicationWrapper } from 'containers/ApplicationWrapper';

function applicationRenderer(location, chunkExtractor) {
	const applicationString = renderToString(
		<ChunkExtractorManager extractor={chunkExtractor}>
			<StaticRouter location={location}>
				<ApplicationWrapper />
			</StaticRouter>
		</ChunkExtractorManager>
	);

	const scriptTags = chunkExtractor.getScriptTags();
	const styleTags = chunkExtractor.getStyleTags();
	const cssString = chunkExtractor.getCssString();

	return {
		applicationString,
		scriptTags,
		styleTags,
		cssString
	};
}

export default applicationRenderer;
