import React from 'react';
import Icon from '../../assets/images/heart.svg';

import './HomePage.scss';

function HomePage() {
	return (
		<div className="home-page">
			hello world
			{' '}
			<img className="icon svg" alt="icon" src={Icon} />
		</div>
	);
}

export default HomePage;
