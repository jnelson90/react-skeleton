import React from 'react';
import { shallow } from 'enzyme';
import App from '../App';

describe('Containers | App', () => {
	it('renders the ApplicationWrapper', () => {
		const component = shallow(
			<App />
		);

		expect(component).toMatchSnapshot();
	});
});
