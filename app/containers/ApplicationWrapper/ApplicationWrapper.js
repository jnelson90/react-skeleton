import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import Routes from 'routes';

function ApplicationWrapper() {
	return (
		<Fragment>
			<div className="header">
				<Link to="/">Home</Link>
				{' | '}
				<Link to="/test">Test</Link>
			</div>
			<Routes />
			<div className="footer">Footer</div>
		</Fragment>
	);
}

export default ApplicationWrapper;
