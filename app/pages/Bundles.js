import loadable from '@loadable/component';

export const HomePage = loadable(() => import(/* webpackChunkName: "HomePage" */'./HomePage/HomePage'));
export const TestPage = loadable(() => import(/* webpackChunkName: "TestPage" */'./TestPage/TestPage'));
