import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ApplicationWrapper } from 'containers/ApplicationWrapper';

import '../main.scss';

function App() {
	return (
		<BrowserRouter>
			<ApplicationWrapper />
		</BrowserRouter>
	);
}

export default App;
