const { mergeWithCustomize, customizeArray } = require('webpack-merge');
const common = require('./client.common');
const hash = require('./hash');

const publicPath = `https://static.storage.com/assets/${hash}`;

const configuration = mergeWithCustomize({
	customizeArray: customizeArray({
		'output.publicPath': 'prepend'
	})
})(common, {
	mode: 'production',
	output: {
		publicPath: `${publicPath}/`
	},
	devtool: 'source-map'
});

module.exports = configuration;
