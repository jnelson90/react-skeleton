const fs = require('fs');
const paths = require('./paths');

let hash = 'development';

try {
	hash = fs.readFileSync(`${paths.PROJECT_DIR}/hash`).toString().replace(/\n/g, '');
} catch (err) { }

module.exports = hash;
