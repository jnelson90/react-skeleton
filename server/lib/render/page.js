const isDevelopment = process.env.NODE_ENV === 'development';

export default function Page({
	applicationString, preloadedState, scriptTags, styleTags, cssString
}) {
	// Use link tags for dev and inline styles for prod
	const stylePromise = isDevelopment
		? Promise.resolve(styleTags)
		: cssString.then((inlineCssStr) => `<style type="text/css">${inlineCssStr}</style>`);

	return stylePromise.then((styles) => `<!DOCTYPE html>
		<html lang="en" xmlns:fb="http://ogp.me/ns/fb#">
		<head>
			<meta charset="utf-8"/>
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2" />
			${styles}
		</head>
		<body>
			<div id="app">${applicationString}</div>
			<script type="text/javascript">
				window.__PRELOADED_STATE__ = ${JSON.stringify(preloadedState).replace(/</g, '\\u003c')};
			</script>
			${scriptTags}
		</body>
	</html>`);
}
