import React from 'react';
import { hydrate } from 'react-dom';
import { loadableReady } from '@loadable/component';
import App from 'containers/App';

// Grab the state from a global variable injected into the server-generated HTML
// const preloadedState = window.__PRELOADED_STATE__;

// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__;

const render = (AppComponent) => hydrate(<AppComponent />, document.getElementById('app'));

loadableReady(() => render(App));

if (module.hot) {
	module.hot.accept(['./containers/App'], () => {
		const NextApp = require('./containers/App').default; /* eslint-disable-line global-require, max-len */
		render(NextApp);
	});
}
