const { Composer } = require('@sparefoot/config');
const webserver = require('./webserver');

const config = new Composer({
	envPrefix: 'Storage_'
});

config.setConfig(webserver, 'webserver');

module.exports = config.getConfig();
